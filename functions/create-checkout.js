const Stripe = require('stripe');
const fetch = require('node-fetch');

// Secret key to identify the site to hCaptcha
const hcaptcha_key = process.env.HCAP_SECRET_KEY;

// Key name for captcha passsword response
const hcaptcha_response_key = process.env.HCAP_RESPONSE_KEY

// Verification endpoint for CAPTCHA
const hcaptcha_verify_url = process.env.HCAP_VERIFY_URL

const nonprofit_classes = {
  "501c3": {
    pub_key: process.env.STRIPE_PUBLISHABLE_KEY_501C3,
    secret_key: process.env.STRIPE_SECRET_KEY_501C3,
    product: process.env.PRODUCT_501C3,
  },
  "501c4": {
    pub_key: process.env.STRIPE_PUBLISHABLE_KEY_501C4,
    secret_key: process.env.STRIPE_SECRET_KEY_501C4,
    product: process.env.PRODUCT_501C4,
  },
};

const payment_frequency = {
  onetime: {},
  monthly: {
    recurring: {
      interval: 'month',
      interval_count: 1,
    },
  },
  yearly: {
    recurring: {
      interval: 'year',
      interval_count: 1,
    },
  },
};

function http_error(code, message) {
  console.error(message);
  return {
    statusCode: code,
    body: JSON.stringify({ message }),
  };
}

exports.handler = async (req) => {
  let { amount, nonprofit_class, email, frequency, ...body } = JSON.parse(req.body);
  const { pub_key, secret_key, product } = nonprofit_classes[nonprofit_class] || {};
  const captcha_response = body[hcaptcha_response_key];

  if (product === undefined) {
    return http_error(400, "Nonprofit choice is invalid.");
  }

  if (isNaN(amount)) {
    return http_error(400, "Price is not a number.");
  }

  if (captcha_response) {
    const verify_request_data = new URLSearchParams({
      "response": captcha_response,
      "secret": hcaptcha_key,
    });

    const verify_response = await fetch(hcaptcha_verify_url, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: verify_request_data
    });

    const verify_response_json = await verify_response.json();

    // Boot out the robots
    if (!verify_response_json["success"]) {
      return http_error(401, "Captcha response was not valid.")
    }
  }
  else {
    return http_error(401, "Captcha has not been attempted.")
  }

  amount = Math.round(amount * 100); // because JS is dumb and the math turns into a floating point error

  const stripe = Stripe(secret_key);

  const stripe_session = await stripe.checkout.sessions.create({
    mode: frequency === 'onetime' ? 'payment' : 'subscription',
    customer_email: email,
    billing_address_collection: 'auto',
    success_url: 'https://www.fighttorepair.org/checkout/success',
    cancel_url: 'https://www.fighttorepair.org/checkout/oops',
    line_items: [
      {
        quantity: 1,
        price_data: {
          currency: 'usd',
          unit_amount: amount,
          product,
          ...payment_frequency[frequency],
        },
      }
    ],
  });

  return {
    statusCode: 200,
    body: JSON.stringify({
      sessionId: stripe_session.id,
      publishableKey: pub_key,
    }),
  };
};
