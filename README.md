## Run the site locally

 - Install yarn v1 https://classic.yarnpkg.com/en/docs/install
 - Add yarn's global install location to your path, see instructions here https://classic.yarnpkg.com/en/docs/cli/global#adding-the-install-location-to-your-path
 - Run the following commands with your terminal/shell in the project directory
   ```sh
   yarn global add netlify-cli hugo-bin # only need to do this once
   yarn # run this to install or update dependencies
   ntl dev # run this to start the local version of the site
   ```

### Testing stripe payments

Before running the site, create a `.env` file in this directory with contents
like this

```
# Global test keys for hcaptcha
HCAP_SITE_KEY=10000000-ffff-ffff-ffff-000000000001
HCAP_SECRET_KEY=0x0000000000000000000000000000000000000000
HCAP_RESPONSE_KEY=h-captcha-response
HCAP_VERIFY_URL=https://hcaptcha.com/siteverify

STRIPE_PUBLISHABLE_KEY_501C3=pk_test_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
STRIPE_SECRET_KEY_501C3=sk_test_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
PRODUCT_501C3=prod_YYYYYYYYYYYYYYYYYYYYYYYYYYYYY

STRIPE_PUBLISHABLE_KEY_501C4=pk_test_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
STRIPE_SECRET_KEY_501C4=sk_test_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
PRODUCT_501C4=prod_YYYYYYYYYYYYYYYYYYYYYYYY
```

Get the publishable, secret key and product values from your own stripe account

 - Get the API keys here https://dashboard.stripe.com/test/apikeys
 - Get product IDs here https://dashboard.stripe.com/test/products?active=true
 - See the example in [Stripe integration](#stripe-integration) if you can't find the product ID

You should be able to open a test version of the donation page from your local
version of the site. Run the site locally as normal with `ntl dev` in this
directory, click on `Donate` in the upper right of the site and choose a 501 to
start a test donation. Use credit card 4242 4242 4242 4242 to attempt a test
payment. See more about testing Stripe here https://stripe.com/docs/testing

### Updating dependencies

Run `yarn upgrade` in this directory, if `package.json` or `yarn.lock` are
upgraded, commit them and submit a Merge Request to this repository.

## Blog and Authors

To edit to the blog, visit https://fighttorepair.org/admin/ and login with your
registered credentials.

To manage blog authors, go to our team at https://app.netlify.com and open the
`Identity` settings under our site. To add new authors, click the `Invite users`
button and add their email addresses. They will be sent a registration email,
with options for password, gitlab, or google authentication, any choice of auth
from these will work.

If needed, password reset emails can also be sent from this page, click on the
user and click `Send reset password email` so they may re-enroll their
credentials. To remove authors, click on the user and click the `Delete user`
button at the bottom of the page.

## Website Setup

There may be occasions where the site may need to be partially or entirely
re-configured. This guide will show how to create and configure the site from
start to finish, and replacing the old version.

This guide should only be necessary for when something goes wrong, normally the
site is always built and deployed from the main branch automatically.

### Initial steps

Go to https://app.netlify.com and sign in to our team, then go to `Sites` and click `New site from Git`

![new site](assets/playbook/new_site_start.png)

Choose to deploy from GitLab, then click on the dropdown with your username and
choose the `Repair Preservation Group Action Fund` org from the list. Click on
the `website` repository.

![choose repo](assets/playbook/choose_repo.png)

Scroll to the bottom of the page and click `Deploy Site`, leave everything else
alone for now.

Choose the new site from the `Sites` section, and verify these settings under `Site settings`:

 - `Build & deploy -> Continuous Deployment -> Branches` -- click `Edit Settings`
   and verify that `Branch Deploys` is set to `None`, then click `Save`
 - `Build & deploy -> Continuous Deploument -> Deploy Previews` -- click `Edit
   Settings` and set `Deploy Previews` to `None`, then click `Save`
 - `Identity` -- enable the Identity service if it is not already
 - `Identity -> Registration` -- click `Add Provider` and add the GitLab and
   Google auth providers
 - `Identity -> Emails` -- Make sure all of the email subject templates refer to
   fighttorepair.org
 - `Identity -> Services` -- Turn on the Git Gateway

### Stripe integration

If the current keys and settings are still valid from the previous deploy,
simply visit the old site on the netlify dashboard and go to `Site Settings ->
Build & deploy -> Environment`, click on `Edit variables` and copy all the
entries to the corresponding page of the new site.

If the keys or products are no longer valid, visit
https://dashboard.stripe.com/apikeys for the 501c3 and 501c4 one at a time and
complete the following steps for each, click on the dropdown in the upper left
of the page to switch organizations.

Create a new secret key for each by clicking on `Create Secret Key`, name it
`fighttorepair.org`. **Copy and paste the keys to the new site environment as
you go**, they will only be visible in the stripe dashboard **once**, otherwise
you will need to create another one. After copying & pasting the secret key, do
the same for the publishable key. Enter the publishable and secret keys for each
fund with the following key names:

 - `STRIPE_PUBLISHABLE_KEY_501C3`
 - `STRIPE_SECRET_KEY_501C3`

 - `STRIPE_PUBLISHABLE_KEY_501C4`
 - `STRIPE_SECRET_KEY_501C4`

Similarly, get the product IDs for the 501c3 and 501c4 from
https://dashboard.stripe.com/products?active=true for each organization. Click
on the `Donate to Repair Preservation Group...` product and see the `Details`
heading for the ID, it will begin with `prod_`.

![product](assets/playbook/product.png)

Enter them with these key names in the environment:

 - `PRODUCT_501C3`
 - `PRODUCT_501C4`

The environment table should look like this in the end, make sure the names are
appropriately entered as above. Click `Save` when finished.

![env](assets/playbook/env.png)

### Verifying everything works

Go to the `Deploys` tab of the new site in the netlify dashboard, and click on
the `Trigger deploy` dropdown, then click `Deploy site`. Wait for the
`Published` label to appear on the deploy you triggered.

![pub deploy](assets/playbook/pub_deploy.png)

Once you see the label appear, click on the link below the `Deploys for [site
name]` heading. This will take you to the new version of the site. Make sure you
can visit the blog section & see the blog posts, then click on `Donate` in the
upper right corner of the page and begin a donation for each 501. Ensure that
the merchant displayed for each 501 is properly displayed in the upper left at
checkout, see the screenshot here for reference. If you see the wrong 501 at
checkout, check the environment settings again, and repeat the steps under
[Stripe integration](#stripe-integration)

![checkout](assets/playbook/checkout.png)


### Cutting over

Once you have verified the critical functionality, go to the old version of the
site in the netlify dashboard, go to `Site settings -> Domain management` and
remove the fighttorepair.org domain by clicking on the `Options` dropdown next
to it, and click `Remove domain`. Then, go to the new site under `Site settings
-> Domain management`, and click `Add custom domain`. Add `fighttorepair.org`,
and scroll down to check that HTTPS is enabled, turn it on if it is not.

Finally, repeat the steps in [verifying everything works](#verifying-everything-works) including
re-deploying the site. You should be directed to https://fighttorepair.org if
not, check the domain settings for the site again. If everything is working, it
is safe to delete the old site by going to its dashboard under `Site settings ->
General -> Danger zone`, then click `Delete this site` and follow the steps to
finish.

### Final housekeeping

When the site is deleted, any existing blog authors will be removed. Make sure
the regular authors are re-added using the instructions in [Blog and Authors](#blog-and-authors)
