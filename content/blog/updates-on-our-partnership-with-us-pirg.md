---
title: Updates on our partnership with US PIRG
date: 2022-04-15T21:12:36.189Z
draft: false
featureImage: https://public.inditioncrm.com/3c540423-f639-45fe-bba3-8e685025bca0/1509048290423_USPIRG.jpg
postImage: https://public.inditioncrm.com/3c540423-f639-45fe-bba3-8e685025bca0/1509048290423_USPIRG.jpg
---
Today, I'd like to go over some updates from our partnership with USPIRG. They've had advocates in just about every state I have been to in my travels testifying at, and recording, Right to Repair hearings around the country. It was my goal to be able to bolster the efforts of people doing good work in this space, so that it is not just me doing the work. Here's what they've accomplished over the past few months. 

* We successfully ramped our shareholder activism (c3). 
* * SUMMARY: We ramped up our mobilization of shareholders around Right to Repair, noting that these commitments tend to soften the resistance to legislation (such as Microsoft), and undermine opposition arguments. 
  * Apple DIY rapid response. We generated close to 600 media hits on this 
  * Deere shareholder dust-up appears to be a real dent in the armor 
  * Google created a repair program amid shareholder activism, it appears at least the timing was driven by shareholders.  
  * Dell negotiated some changes with us, being announced in the next few days. 
  * * More about our shareholder work:
    * <https://uspirg.org/blogs/blog/usp/deere-shareholders-it-doesnt-add-up>
    * <https://uspirg.org/blogs/blog/usp/examining-google%E2%80%99s-repair-track-record-shareholders-call-change>
    * <https://uspirg.org/blogs/blog/usp/why-dell-didnt-get-an-a-our-repair-scorecard>
    * <https://uspirg.org/news/usp/statement-amid-shareholder-efforts-and-legislative-push-google-announces-new-repair-access>  
* We deepened our relationship with the White House pro-competition team, and kept them thinking about Right to Repair. 
* * Summary: The White House competition council (Tim Wu), is a critical ally for us in advancing Right to Repair and elevating it as an issue. Nathan appeared at a public event with the White House in December. 
  * At our public meeting, we were able to get the white house on record around medical devices which we need to help us with the FDA.
  * Biden spoke publicly about Right to Repair in February. If the administration takes more ownership of the issue, it helps us with the FTC
* We hosted a lawmaker summit to help ramp up states and train our champions. 
* * We recruited 35 lawmakers, states reported a lot of momentum coming out of that. Good to connect lawmakers with experts -- help shore up a number of states on some of the thorny issues . 
  * Keynote given by Adam Savage.
* We did a farmer survey with Farmers Union (c3), and unveiled it when Tester dropped his bill. 
* * We focused coverage on this feature piece on NBC: <https://www.nbcnews.com/tech/new-senate-bill-farm-equipment-right-to-repair-rcna13961> 
  * We were able to use some of the resources to survey and interview about 2 dozen farmers. 
  * Jared Wilson, a farmer from Missouri has been increasingly important leader for us (would be a good guest on your channel). 
  * More about our survey: <https://uspirg.org/news/usf/repair-restrictions-come-home-roost-study-shows-farmers-want-fix-their-own-tractors> 
* We held a roundtable for leaders around medical device repair, and are working to get cost data out of VA hospitals to help make the case on Medical Right to Repair 
* We released a new Deere in the Headlights report (c3), which accompanies a formal FTC complaint against Deere 
* * Excellent media coverage in IL, and it seems like our report, which adds a new analysis for dealer consolidation, is a really important aspect to farmers. They feel like their choices are gone. 
  * FTC complaint feels like a bullseye -- much different reception than Apple complaint. We continue to organize farmers to talk to the FTC on a regular basis, and hope this complaint is fruitful. 
* We released a new repair scorecard (c3) 
* * Our state groups helped events across the country, while while the media is still coming in, we have about 40 pieces generated so far. 
  * We have spent some extra to translate materials into spanish and work spanish media -- starting to pay off: <https://www.latercera.com/que-pasa/noticia/sabe-cuales-son-los-celulares-y-computadores-mas-dificiles-de-reparar-un-estudio-investigo-que-marcas-son-mas-complejas-de-arreglar/YQ4N437TTBG5FHRL4VPKQ4EOHQ/>
* We organized a massive congressional lobby day on Right to Repair 
* * We held 130 lobby meetings, including 12 directly with members of Congress. Right to Repair was a main topic of conversation. 
  * We worked with Goodfriend group to deliver a letter calling for a hearing (signed by RPGAF and others). 
  * After the lobby day, the Judiciary staff committed to holding a Right to Repair hearing.