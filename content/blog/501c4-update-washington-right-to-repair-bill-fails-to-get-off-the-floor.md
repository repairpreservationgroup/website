---
title: "501c4 update: Washington Right to Repair bill fails to get off the floor"
date: 2022-02-15T22:04:51.282Z
draft: false
featureImage: https://pbs.twimg.com/profile_images/1027621963437506560/-hY4uSEv_400x400.jpg
postImage: https://pbs.twimg.com/profile_images/1027621963437506560/-hY4uSEv_400x400.jpg
---
Last year, we announced our [partnership with WashPIRG & Craig Engelking](https://fighttorepair.org/blog/501c4-update-partnership-with-washpirg-on-right-to-repair-in-washington/) to try and get a bill pushed forward in Washington. Louis has [testified before in Olympia](https://www.youtube.com/watch?v=oLIW7mQ8CI4) with some [very pointed critiques](https://youtu.be/-vmbDczK-78) of the bizarre testimony fro many industry lobbyists. 

While Steve Kirby was an amazing ally in getting this bill as far as it got, we were unable to go further. A big part of the problem were the usual suspects - Technet, CTA, Apple, all being against the bill. Specifically, introducing very wacky amendment suggestions at the last minute to muddle the water.

One example was a requirement that schools and students not use third party repair due to data security & privacy as an amendment, as an attempt to raise data security as an issue. The idea here is that the 3rd parties might steal people's data while fixing student laptops. We all know independent repair shops don't exist to make money by quickly & efficiently repairing your devices, but rather, to steal your homework. Right. 

Sadly, the legislators bought it. It was just enough to create doubt in their mind about moving forward. I had higher hopes after listening to the responses from Senators [Marko Liias](https://youtu.be/-vmbDczK-78), [Derek Stanford](https://youtu.be/kd3Qa9tlA3o), and [Doug Ericksen](https://www.youtube.com/watch?v=6n9dqBP0-N0); who all seemed to see through this nonsense. Perhaps that was unfounded. 

We do wonder if it would've been easier to address these concerns in person, and have a real discussion rather than just email & zoom. The legislature was not open to the public due to COVID. Next year!