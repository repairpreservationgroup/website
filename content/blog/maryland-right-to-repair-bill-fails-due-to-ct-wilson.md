---
title: Maryland Right to Repair bill fails due to CT Wilson
date: 2022-03-24T20:13:56.066Z
draft: false
featureImage: https://upload.wikimedia.org/wikipedia/commons/c/c0/1ct_wilson.jpg
postImage: https://upload.wikimedia.org/wikipedia/commons/c/c0/1ct_wilson.jpg
---
I think this was best summarized by [TheBaenAddict on reddit](https://www.reddit.com/user/TheBaenAddict/). 

> Louis Rossmann, a prominent MacBook repair tech and advocate for the right of people to repair their equipment and electronics lobbied in favor of a bill on the topic in Maryland a couple years back. That bill would have required manufacturers to provide schematics identifying the individual parts inside their product and offer those parts for sale. CT Wilson, the delegate for the 28th district claimed this meant that companies would have to share the source code for their software and that this would lead to viruses. Louis called him out on this by recording what Wilson said and posting it with mild commentary, like he has for many legislators and lobbyists.
>
> CT Wilson responded in the hearing for the most recent version of that bill by calling Louis's video racist and claiming he (Wilson) had received a single anonymous threatening email. He now opposes the environmental, financial, and personal freedom values of right to repair because he is angry about being shown to be uninformed and foolish.

I describe the issue in detail [here](https://www.youtube.com/watch?v=b9HyGvqFL0I).

We had [hired a team](https://fighttorepair.org/blog/501c4-update-maryland-advocacy-work-to-be-done/) to try and make progress with this bill last year. We did our best to work this issue out. We addressed it early on, with an offer to come and personally apologize to the committee chair who was offended by my [initial video](https://www.youtube.com/watch?v=ej1MmjCPYqU&t=0s). He had said this is unnecessary, yet came out and publicly made a [baseless accusation](https://www.youtube.com/watch?v=k3I-lHECIqo) against us & moved forward. 

I think we did what we could in Maryland, and made a best-effort to mend any bridges. Until there is new representation in Maryland, or a new committee chair, I feel there is little chance for progress in the state on Right to Repair. It is my personal opinion that he made this accusation to deflect from the fact that he was called out for not understanding the bill's text regarding source code, and that he did not understand how viruses are created. I imagine he received many emails from people who were angry at his blatant dismissal of the text in the bill, and rather than admit he was wrong, fabricated a scenario whereby he could play the victim.

Sometimes you win, sometimes you lose.