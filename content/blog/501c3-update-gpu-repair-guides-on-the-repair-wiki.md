---
title: "501c3 update: GPU repair guides on the repair wiki"
date: 2021-11-11T01:26:33.317Z
draft: false
featureImage: /images/uploads/gpu.jpg
postImage: /images/uploads/gpu.jpg
---
[NVIDIA GPU repair](https://repair.wiki/w/Nvidia_Pascal_GPU_Diagnosing_Guide_(1060-1080ti)) and [AMD GPU repair](https://repair.wiki/w/AMD_Polaris_GPU_Diagnosing_Guide) now have a home on repair.wiki, courtesy of Ahmed. There you will find detailed troubleshooting guides for use in repairing GPUs at component level. GPUs are becoming increasingly more expensive, and when they die, are being thrown away instead of repaired.

When the main BGA chip is at fault, you might not have a source for a replacement. However, when there are other  problems, they can be repairable - and we're happy to facilitate information to the public on how to get that done.