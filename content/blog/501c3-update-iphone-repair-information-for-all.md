---
title: "501c3 update: iPhone repair information for all"
date: 2021-09-28T00:09:38.965Z
draft: false
featureImage: /images/uploads/iphonewiki.jpg
postImage: /images/uploads/iphonewiki.jpg
---
repair.wiki was intended to be a repository for all, on how to make repairs on popular modern devices more economically viable. As of today, it now continues with [some of the most detailed iPhone motherboard troubleshooting guides online](https://repair.wiki/w/Apple_iPhone_series). 

iFixit is an amazing resource. They have countless guides on how to repair items designed & manufactured by companies that release no information to the public on how to disassemble or repair them. One area where iFixit doesn't necessarily focus, is on component level repair. Component level repair is often necessary to make a repair economically viable.

Knowing how to replace a board is a valuable skill - but replacing a $500 board in a $550 product usually means that device is going into a drawer, or a garbage somewhere. The ability to make the repair doable at component level, saving the cost of a replacement board, makes that an economically viable repair again - increasing the likelihood that the repair will be done, rather than the device ending up in a landfill. 

These repairs are more difficult, and often the technicians with the skillsets to figure them out hold their secrets close to the vest. Repair Preservation Group is prepared to retain technicians with top notch skillsets in specific fields, doing advanced repairs on devices of public interest, and contract them to do a brain dump of everything they know. Today, [Jesse Cruz of VCC Board Repairs](https://vccboardrepairs.com/about/) did that for the iPhone. From iPhone 7 to 11, incredibly detailed guides on how to perform board repairs of all kinds.