---
title: "501c4 update: Partnership with WashPIRG on Right to Repair in Washington"
date: 2021-07-08T00:59:49.714Z
draft: false
featureImage: https://pbs.twimg.com/profile_images/1027621963437506560/-hY4uSEv_400x400.jpg
postImage: https://pbs.twimg.com/profile_images/1027621963437506560/-hY4uSEv_400x400.jpg
---
Anyone who watched the [Right to Repair hearing](https://www.youtube.com/watch?v=FBR8IvXVwsE) last year may wonder, why support for the Right to Repair bill wavered. The senators brought up excellent points & asked questions that made it clear they understood the problem. However, at the end of the day, in spite of the inaccurate arguments made by the opposition, the bill went nowhere. 

Repair Preservation Group Action Fund is issuing [WashPIRG](https://washpirg.org/) a grant so that they may maintain a lobbying presence in the state to gain greater understanding into what will result in the passage of a successful bill. This is to begin more towards the end of the year. We will gain an understanding of what is going on throughout the legislative process, and ensure our voice is heard.