---
title: "Coming soon: Major updates on lobbying and educational efforts"
date: 2021-12-02T12:49:27+00:00
draft: false
featureImage: images/uploads/soder.jpg
postImage: images/uploads/soder.jpg
---
This website, [fighttorepair.org](https://fighttorepair.org), has recently been overhauled. We will now be publishing more frequent updates on Right to Repair (R2R) education and lobbying activity when possible. I have backdated the posts to reflect when these were initially written, as they were not posted to the site at the time. 

We also intend to announce some very important updates:

* We have retained lobbyists in many states, and will be making an announcement in the near future to explain what this means for Right to Repair.
* We are making some major changes to the structure and organization of the [Repair Preservation Group Wiki](https://repair.wiki). We will need your help to improve and reorganize content! Stay tuned for more information.