---
title: "We now use Stripe to accept donations"
date: 2021-12-01T12:49:27+00:00
featureImage: images/uploads/donate.png
postImage: images/uploads/donate.png
---

Prior to now, most public donations to Repair Preservation Group and Repair
Preservation Group Action Fund have been collected through PayPal and GoFundMe
respectively. As of today, both organizations can now accept donations made through Stripe,
which offers growing choices for payment methods, and payment frequencies for recurring donations.

Try it out for yourself! Click on the 'Donate' button in the top-right corner
of the page, and choose which fund you'd like to donate to. As always, thank
you for your support!
