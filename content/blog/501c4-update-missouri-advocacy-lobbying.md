---
title: "501c4 update: Missouri advocacy & lobbying with the Rowden Group"
date: 2021-09-16T21:16:06.849Z
draft: false
featureImage: /images/uploads/unnamed.png
postImage: /images/uploads/unnamed.png
---
Today we are retaining the Rowden Group. A Right to Repair bill got out of committee in Missouri thanks to a strong sponsor. Missouri has the strongest Republican sponsor who has dealt with national media, and bill sponsors believe that the Rowden Group would be excellent partners in helping push Right to Repair forward in the state. We look forward to working with them to identify neutral groups, opposition, allies, and come up with a plan to get something passed.