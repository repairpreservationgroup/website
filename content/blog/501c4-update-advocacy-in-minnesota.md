---
title: "501c4 update: lobbying in Minnesota with Fredrikson & Byron"
date: 2021-10-05T20:49:33.586Z
draft: false
featureImage: https://www.fredlaw.com/wp-content/themes/fredlaw-migration/amm/themes/fredlaw_2016/images/logo.png
postImage: https://www.fredlaw.com/wp-content/themes/fredlaw-migration/amm/themes/fredlaw_2016/images/logo.png
---
Today we’ve engaged a lobbying contract with [Fredrikson & Byron, PA](https://www.fredlaw.com/) in Minnesota. There is a lot of support for Right to Repair in this state – both for agriculture and for electronics. The finance chair of the state GOP refurbishes servers. They know what we're talking about. 

Right to Repair hit a wall in the form of a [senator that ran car dealerships](https://www.youtube.com/watch?v=E1p0WOl25nQ&t=1s) who is said to have seen what poor quality shops do to cars, and is not for Right to Repair. We are exploring whether there is a way to make progress regardless, as there is a lot of support in the state. There are strategies to getting Right to Repair pushed through without his support, and also ways to send the message that this issue will not go away.

We look forward to working together to see what is possible.