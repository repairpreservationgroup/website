---
title: "501c4 update: Illinois lobbying with Lowitzki Consulting"
date: 2021-10-01T21:43:22.050Z
draft: false
featureImage: https://lh3.googleusercontent.com/proxy/5CwaHKf7h06W3jTddxprDY9GF_F21qMJRrQ_NkJEvLjd1kQfBocPcg8MVVRdEvLyV-ZX0zEFhXSZ0-oUwKjGWkumozC1cEEfrXv21Tik5nnGbCXJKkS9n57-XLlJx4xGRKk
postImage: https://lh3.googleusercontent.com/proxy/5CwaHKf7h06W3jTddxprDY9GF_F21qMJRrQ_NkJEvLjd1kQfBocPcg8MVVRdEvLyV-ZX0zEFhXSZ0-oUwKjGWkumozC1cEEfrXv21Tik5nnGbCXJKkS9n57-XLlJx4xGRKk
---
We have retained the firm [Lowitzki Consulting](http://www.lowitzkiconsulting.com/about) to assist us in finding a path forward for Right to Repair in Illinois. While the Farm Bureau & Aurora Health are  strong partners in the state, we do need to make sure that we have more than just coalition partners; actual elected individuals as bill sponsors would be nice!

I look forward to seeing what they & the folks from Illinois PIRG come up with regarding finding sponsors & forging a path for Right to Repair in Illinois.