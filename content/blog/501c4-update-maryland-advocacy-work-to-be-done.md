---
title: "501c4 update: Maryland lobbying with Funk & Bolton"
date: 2021-09-17T21:33:46.934Z
draft: false
featureImage: https://www.mdmunicipal.org/ImageRepository/Document?documentId=7723
postImage: https://www.mdmunicipal.org/ImageRepository/Document?documentId=7723
---
In March of 2020, the senate chair [asked an anti-repair lobbyist if she was a "slave to your operation"](https://www.youtube.com/watch?v=_3ebHxcQ_8U) - there is clearly potential here for Right to Repair to move forward. The bill [didn't do as well in the House](https://www.youtube.com/watch?v=ej1MmjCPYqU), but it's still likely we can progress. Maryland PIRG has recommended Funk & Bolton to assist us in recruiting new house sponsors and find a way forward in the senate. It might require a narrower bill, but it would be progress vs. where we are now, which is nothing at all. 

We have retained Funk & Bolton and look forward to forging a path forward for Right to Repair in Maryland.