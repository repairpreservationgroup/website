---
title: "501c4 update: Advocacy in Georgia with Eternal Vigilance Action"
date: 2021-09-23T21:54:28.983Z
draft: false
featureImage: https://i2.wp.com/eternalvigilance.us/wp-content/uploads/2021/09/My-Post.png?resize=1024%2C853&ssl=1
postImage: https://i2.wp.com/eternalvigilance.us/wp-content/uploads/2021/09/My-Post.png?resize=1024%2C853&ssl=1
---
Today we are partnering with an ex-legislator in Georgia, [Scot Turner](https://eternalvigilance.us/), on Right to Repair, who has carried this bill in the past while in office. He has refurbished servers and had issues being locked out by Oracle, which led him to having a greater understanding of the importance of Right to Repair. 

We hope to work with possible bill sponsors to introduce Right to Repair legislation in Georgia. We wish to engage the media, build a coalition, and lay the groundwork for a piece of Right to Repair legislation. We look forward to working with you!